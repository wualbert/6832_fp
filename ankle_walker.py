# -*- coding: utf8 -*-

import argparse
import math
import os.path
import time

import numpy as np

from pydrake.all import (VectorSystem)

class AnkleWalker(VectorSystem):
    def __init__(self, ankleWalker,
        desired_lateral_velocity = 0.0,
        print_period = 0.0):
        '''
        Controls a planar hopper described
        in ankle_walker.sdf.

        :param hopper: A pydrake RigidBodyTree() loaded
            from ankle_walker_2d.sdf.
        :param desired_lateral_velocity: How fast should the controller
            aim to run sideways?
        :param print_period: If nonzero, this controller will print to
            the python console every print_period (simulated) seconds
            to indicate simulation progress.
        '''
        VectorSystem.__init__(self,
            14, # 10 inputs: x, z, theta, alpha, l, and their derivatives
            4) # 2 outputs: Torque on thigh, and force on the leg extension
               #  link. (The passive spring for on the leg is calculated as
               #  part of this output.)
        self.ankleWalker = ankleWalker
        self.desired_lateral_velocity = desired_lateral_velocity
        self.print_period = print_period
        self.last_print_time = -print_period
        # Remember what the index of the foot is
        self.foot_body_index = [0]*2
        self.foot_body_index[0] = ankleWalker.FindBody("footP").get_body_index()
        self.foot_body_index[1] = ankleWalker.FindBody("footN").get_body_index()

        # Default parameters for the ankleWalker -- should match
        # ankle_walker_1d.sdf, where applicable.
        # You're welcome to use these, but you probably don't need them.
        self.ankleWalker_leg_length = 1.0
        self.m_b = 1.0
        self.m_f = 0.1
        self.l_max = 0.5

        #Code for controller
        self.desiredAlpha = 0           #target alpha in flight
        self.legWasStance = [False]*2
        self.legStrikeLength = [None]*2
        self.legHoldLength = [True]*2

    def ChooseHipTorque(self,X):
        '''
        Given system state X, returns torque values of the hip
        :param X: numpy array, length 14, full state of ankleWalker.
        :return: array [torqueP, torqueN]
        '''
        #unpack states
        alpha=np.zeros(2)
        l=np.zeros(2)
        alphaDot = np.zeros(2)
        lDot = np.zeros(2)
        x, y, theta, alpha[0], alpha[1], l[0], l[1] = X[0:7]
        xDot, yDot, thetaDot, alphaDot[0], alphaDot[1], lDot[0], lDot[1] = X[7:]
        #print(l)
        # Run out the forward kinematics of the robot
        # to figure out where the foot is in world frame.
        yTolerance = 0.05
        kinsol = self.ankleWalker.doKinematics(X)

        #get feet contact status
        foot_point=[]
        foot_point_in_world=[]
        foot_in_contact=[]*2
        for i in range(2):
            foot_point.append(np.array([0.0, 0.0, -self.ankleWalker_leg_length]))
            foot_point_in_world.append(self.ankleWalker.transformPoints(kinsol,
                                  foot_point[i], self.foot_body_index[i], 0))
            foot_in_contact.append(foot_point_in_world[i][2] <= yTolerance)

        hipTorque = np.zeros(2)
        kp_swing = 2.0
        kd_swing = 0.5
        kp_stance = 0.5
        kd_stance = 0.2
        for i in range(2):
            if not foot_in_contact[i] and foot_in_contact[(i+1)%2]:
                hipTorque[i] = max(min(kp_swing*(-alpha[i])+kd_swing*(alphaDot[i]),0.1),-0.1)
            else:
                hipTorque[i] = -(kp_stance*(-theta)+kd_stance*(-thetaDot))
        return hipTorque

    def ChooseAnkleForce(self,X):
        '''
        Given system state X, returns torque values of the hip
        :param X: numpy array, length 14, full state of ankleWalker.
        :return: numpy array [torqueP, torqueN]
        '''
        #
        #unpack states
        alpha=np.zeros(2)
        l=np.zeros(2)
        alphaDot = np.zeros(2)
        lDot = np.zeros(2)
        x, y, theta, alpha[0], alpha[1], l[0], l[1] = X[0:7]
        xDot, yDot, thetaDot, alphaDot[0], alphaDot[1], lDot[0], lDot[1] = X[7:]
        #print(l)
        # Run out the forward kinematics of the robot
        # to figure out where the foot is in world frame.

        #get feet contact status
        yTolerance = 0.05
        kinsol = self.ankleWalker.doKinematics(X)
        foot_point=[]
        foot_point_in_world=[]
        foot_in_contact=[]
        for i in range(2):
            foot_point.append(np.array([0.0, 0.0, -self.ankleWalker_leg_length]))
            foot_point_in_world.append(self.ankleWalker.transformPoints(kinsol,
                                  foot_point[i], self.foot_body_index[i], 0))
            foot_in_contact.append(foot_point_in_world[i][2] <= yTolerance)
            if (not self.legWasStance[i] or not self.legStrikeLength[i]) and foot_in_contact[i]:
                self.legStrikeLength[i]=l[i]
            self.legWasStance[i]=foot_in_contact
            #if in swing phase, hold length upon foot strike
            if not self.legWasStance[i]:
                self.legHoldLength[i]=True

        ankleForce = np.zeros(2)

        #controller parameters
        lStrike = 0.45
        lSwing = 0
        pushGain = 100
        kp_up_swing = 10
        kd_up_swing = 2
        kp_down_swing = 25
        kd_down_swing = 3

        p_stiff = 1000
        d_stiff = 100

        #determine front foot and back foot
        if alpha[0] > alpha[1]:
            frontFoot = 1
            backFoot = 0
        else:
            frontFoot = 0
            backFoot = 1

        #push off if both legs are on the ground
        if foot_in_contact[0] and foot_in_contact[1] and alpha[frontFoot]<0:
            self.legHoldLength[frontFoot]=True
            self.legHoldLength[backFoot]=False
            ankleForce[backFoot] = pushGain * max(alpha[backFoot], 0)

        #control the foot in the air
        for i in range(2):
            if not foot_in_contact[i] and foot_in_contact[(i+1)%2]:
                self.legHoldLength[i]=False
                if alpha[i]>0:
                    ankleForce[i] += (kp_up_swing * (lSwing - l[i]) + kd_up_swing * (-lDot[i]))
                else:
                    ankleForce[i] += (kp_down_swing * (lStrike - l[i]) + kd_down_swing * (-lDot[i]))

        #very stiff PD controller for maintaining leg length
        for i in range(2):
            if self.legHoldLength[i] and self.legStrikeLength[i]:
                ankleForce[i] = (p_stiff * (self.legStrikeLength[i] - l[i]) + d_stiff * (-lDot[i]))

        # ldes = 0.3
        # ydes = 1.3
        # kp_stance = 30
        # kd_stance = 0.3
        # pushGain = 20
        #
        # kp_swing = 1
        # kd_swing = 0.5
        # if footP_in_contact:
        #     ankleForce[0]=kp_stance*(ydes-y)+kd_stance*lDot[0]
        #     ankleForce[0]+=pushGain*max(alpha[0],0)
        # else:
        #     ankleForce[0]=kp_swing*(-l[0])+kd_swing*lDot[0]
        #     #print(ankleForce[0],l[0],lDot[0])
        #
        # if footN_in_contact:
        #     ankleForce[1]=kp_stance*(ydes-y)+kd_stance*lDot[0]
        #
        return ankleForce

    # def ChooseThighTorque(self, X):
    #     '''
    #     Given the system state X,
    #         returns a (scalar) rest length of the leg spring.
    #         We can command this instantaneously, as
    #         the actual system being simulated has perfect
    #         force control of its leg extension.
    #
    #     :param X: numpy array, length 10, full state of the ankleWalker.
    #
    #     :return: A float, the torque to exert at the leg angle joint.
    #     '''
    #     x, z, theta, alpha, l = X[0:5]
    #     xd, zd, thetad, alphad, ld = X[5:10]
    #
        # # Run out the forward kinematics of the robot
        # # to figure out where the foot is in world frame.
        # kinsol = self.ankleWalker.doKinematics(X)
        # foot_point = np.array([0.0, 0.0, -self.ankleWalker_leg_length])
        # foot_point_in_world = self.ankleWalker.transformPoints(kinsol,
        #                       foot_point, self.footP_body_index, 0)
        # in_contact = foot_point_in_world[2] <= 0.0
    #
    #     # It's all yours from here.
    #     # Implement a controller that:
    #     #  - Controls xd to self.desired_lateral_velocity
    #     #  - Attempts to keep the body steady (theta = 0)
    #
    #     kp = 10             #proportional gain in flight
    #     kd = 10             #derivative gain in flight
    #     output = 0          #control output
    #     #Stance phase
    #     kp2 = 5             #proportional gain in stance
    #     kd2 = 5             #derivative gain in stance
    #     kvel = 2            #offset for acceleration
    #
    #     if in_contact:
    #         self.wasInContact=True                  #system has reached stance phase
    #         output = kp2 * (theta) + kd2 * thetad   #PD control corrects for theta to make body upright
    #
    #     #Flight
    #     else:
    #         if self.wasInContact:                   #system just left stance
    #             self.desiredAlpha = -math.atan2(xd,zd)  #want to land with alpha pointing at CG footprint
    #
    #         # PD control over footprint, plus a small displacement for acceleration/deceleration
    #         output = -kp * (alpha - self.desiredAlpha) - kd * alphad - kvel*(xd-self.desired_lateral_velocity)
    #         self.wasInContact=False                 #system is no longer in stance
    #     #print(self.desiredAlpha)
    #     return output


    def _DoCalcVectorOutput(self, context, u, x, y):
        '''
        Given the state of the ankleWalker (as the input to this system,
        u), populates (in-place) the control inputs to the ankleWalker
        (y). This is given the state of this controller in x, but
        this controller has no state, so x is empty.

        :param u: numpy array, length 10, full state of the ankleWalker.
        :param x: numpy array, length 0, full state of this controller.
        :output y: numpy array, length 2, control input to pass to the
            ankleWalker.
        '''

        # The naming of inputs is confusing, as this is a separate
        # system with its own state (x) and input (u), but the input
        # here is the state of the ankleWalker.

        # Print the current time, if requested,
        # as an indicator of how far simulation has
        # progressed.
        if (self.print_period and
            context.get_time() - self.last_print_time >= self.print_period):
            print "t: ", context.get_time()
            self.last_print_time = context.get_time()
        y[:] = np.concatenate((self.ChooseHipTorque(u), self.ChooseAnkleForce(u)))
        # print(y)



from pydrake.all import (DirectCollocation, FloatingBaseType,
                         PiecewisePolynomial, RigidBodyTree, RigidBodyPlant,
                         SolutionResult)
from underactuated import (PlanarRigidBodyVisualizer)

from pydrake.all import (DiagramBuilder, FloatingBaseType, Simulator, VectorSystem,
                        ConstantVectorSource, SignalLogger, CompliantMaterial,
                         AddModelInstancesFromSdfString)
from IPython.display import HTML
import matplotlib.pyplot as plt

'''
Simulates a 2d ankleWalker from initial conditions x0 (which
should be a 10x1 np array) for duration seconds,
targeting a specified lateral velocity and printing to the
console every print_period seconds (as an indicator of
progress, only if print_period is nonzero).
'''
def SimulateAnkleWalker(x0, duration,
        desired_lateral_velocity = 0.0,
        print_period = 0.0):
    builder = DiagramBuilder()

    # Load in the ankleWalker from a description file.
    # It's spawned with a fixed floating base because
    # the robot description file includes the world as its
    # root link -- it does this so that I can create a robot
    # system with planar dynamics manually. (Drake doesn't have
    # a planar floating base type accessible right now that I
    # know about -- it only has 6DOF floating base types.)
    tree = RigidBodyTree()
    AddModelInstancesFromSdfString(
        open("ankle_walker.sdf", 'r').read(),
        FloatingBaseType.kFixed,
        None, tree)

    # A RigidBodyPlant wraps a RigidBodyTree to allow
    # forward dynamical simulation. It handles e.g. collision
    # modeling.
    plant = builder.AddSystem(RigidBodyPlant(tree))
    # Alter the ground material used in simulation to make
    # it dissipate more energy (to make the hopping more critical)
    # and stickier (to make the ankleWalker less likely to slip).
    allmaterials = CompliantMaterial()
    allmaterials.set_youngs_modulus(1E8) # default 1E9
    allmaterials.set_dissipation(1.0) # default 0.32
    allmaterials.set_friction(1.0) # default 0.9.
    plant.set_default_compliant_material(allmaterials)

    # Spawn a controller and hook it up
    controller = builder.AddSystem(
        AnkleWalker(tree,
            desired_lateral_velocity = desired_lateral_velocity,
            print_period = print_period))
    # print(plant.get_input_port(0).size(),"plant input")
    # print(plant.get_output_port(0).size(),"plant output")
    #
    # print(controller.get_input_port(0).size(),"controller input")
    # print(controller.get_output_port(0).size(),"controller output")

    builder.Connect(plant.get_output_port(0), controller.get_input_port(0))
    builder.Connect(controller.get_output_port(0), plant.get_input_port(0))

    # Create a logger to log at 30hz
    state_log = builder.AddSystem(SignalLogger(plant.get_num_states()))
    state_log._DeclarePeriodicPublish(0.0333, 0.0) # 30hz logging
    builder.Connect(plant.get_output_port(0), state_log.get_input_port(0))

    # Create a simulator
    diagram = builder.Build()
    simulator = Simulator(diagram)
    # Don't limit realtime rate for this sim, since we
    # produce a video to render it after simulating the whole thing.
    #simulator.set_target_realtime_rate(100.0) 
    simulator.set_publish_every_time_step(False)

    # Force the simulator to use a fixed-step integrator,
    # which is much faster for this stiff system. (Due to the
    # spring-model of collision, the default variable-timestep
    # integrator will take very short steps. I've chosen the step
    # size here to be fast while still being stable in most situations.)
    integrator = simulator.get_mutable_integrator()
    integrator.set_fixed_step_mode(True)
    integrator.set_maximum_step_size(0.0005)

    # Set the initial state
    state = simulator.get_mutable_context().get_mutable_continuous_state_vector()
    state.SetFromVector(x0)

    # Simulate!
    simulator.StepTo(duration)

    return tree, controller, state_log
